package com.wfis.wfis_shop.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wfis.wfis_shop.R;
import com.wfis.wfis_shop.adapters.ViewPagerAdaper;
import com.wfis.wfis_shop.core.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends BaseFragment {

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    private ViewPager viewPager;
    private View findShop;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        findViews(view);
        setListeners();

        ViewPagerAdaper adaper = new ViewPagerAdaper();
        viewPager.setAdapter(adaper);
        List<String> photos = new ArrayList<>();
        photos.add("https://img.pakamera.net/i1/1/189/grafika-12290206_2008230189.jpg");
        photos.add("http://bi.gazeta.pl/im/44/99/14/z21600580Q,Zakazana-grafika.jpg");
        photos.add("http://zdjecia.nurka.pl/images/www.sylwesteroferty.pl-grafika-kot-sylwester-2.gif");
        adaper.setPhotos(photos);
        return view;
    }

    private void setListeners() {
        findShop.setOnClickListener(view -> {
            getNavigation().changeFragment(ShopMapFragment.newInstance());
        });
    }

    private void findViews(View view) {
        viewPager = view.findViewById(R.id.view_pager);
        findShop = view.findViewById(R.id.find_shop);
    }
}
